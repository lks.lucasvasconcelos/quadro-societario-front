import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { EmpresaModel } from './empresas-crud/empresa.model';


@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  empresasUrl = 'http://localhost:8000/api'


  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<any[]>(`${this.empresasUrl}/empresas`);
  }

  salvar(empresa: EmpresaModel){
    return this.http.post(`${this.empresasUrl}/empresas`, empresa);
  }
}
