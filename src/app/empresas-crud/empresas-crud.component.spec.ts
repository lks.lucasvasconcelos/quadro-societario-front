import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresasCrudComponent } from './empresas-crud.component';

describe('EmpresasCrudComponent', () => {
  let component: EmpresasCrudComponent;
  let fixture: ComponentFixture<EmpresasCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresasCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresasCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
