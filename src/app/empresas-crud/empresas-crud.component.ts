import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../empresa.service'
import { EmpresaModel } from './empresa.model';


@Component({
  selector: 'app-empresas-crud',
  templateUrl: './empresas-crud.component.html',
  styleUrls: ['./empresas-crud.component.css']
})
export class EmpresasCrudComponent implements OnInit {
  empresa = new EmpresaModel();

  empresas: Array<any>;

  constructor(private empresaService: EmpresaService) { }

  ngOnInit() {
    this.listar()
  }

  listar() {
    this.empresaService.listar().subscribe(resp => this.empresas = resp)
  }

  cadastrar(){
    console.log(this.empresa)
    this.empresaService.salvar(this.empresa).subscribe(resp => {})
    this.listar();
  }

}
