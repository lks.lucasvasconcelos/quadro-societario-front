import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SociosCrudComponent } from './socios-crud.component';

describe('SociosCrudComponent', () => {
  let component: SociosCrudComponent;
  let fixture: ComponentFixture<SociosCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SociosCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SociosCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
