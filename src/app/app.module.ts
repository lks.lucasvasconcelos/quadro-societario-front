import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SociosCrudComponent } from './socios-crud/socios-crud.component';
import { EmpresasCrudComponent } from './empresas-crud/empresas-crud.component';
import { SocioService } from './socio.service';
import { EmpresaService } from './empresa.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    SociosCrudComponent,
    EmpresasCrudComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ SocioService, EmpresaService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
